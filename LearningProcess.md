# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### Question 1

### What is the Feynman Technique? Explain in 1 line.

* It is a learning process designed to make us learn things in an efficient way.

## 3. Learning How to Learn TED talk by Barbara Oakley

### Question 2

###  In this video, what was the most interesting story or idea for you?

### There are two modes in our brain.

* Focus Mode
* Diffuse Mode

### Pomodoro Technique

* Set a timer for 25 minutes
* Turn off all the distractions
* Work in Focused Mode for 25 minutes
* After 25 minutes of work, do something fun for few minutes
* This helps with practicing both focused mode of working and relaxation

* Understanding alone isn't enough to build mastery of the material.

### Question 3

### What are active and diffused modes of thinking?

### 1. Active Mode of Thinking: 

* This mode of thinking involves concentrated, focused attention on a specific task or problem. When we are actively thinking, our brain is engaged in a targeted manner. For example, when you're studying for an exam or working on a complex problem at work, you are likely in an active thinking mode.

### 2. Diffuse Mode Of Thinking

* Diffused mode of thinking is characterized by a more relaxed and expansive mental state. whwn we are in a diffused mode we cannot focus on single task. 


## 4. Learn Anything in 20 hours

### Question 4
### According to the video, what are the steps to take when approaching a new topic? Only mention the points.

* Deconstruct the Skill
* Learn enough to self-Correct
* Remove distractions during practice
* Practice consistently everyday
* Practice atleast 45 minutes a day

 
 ### Question 5

### What are some of the actions you can take going forward to improve your learning process?

* Involvement 100%
* Practice Deep work
* Manage Destructions
* Track Our Time
* Understanding software Concepts
* Make Learning Fun and Easy
* Teach Others



