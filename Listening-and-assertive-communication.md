## Listening and Active Communication

### 1. Active Listening

### Question 1

#### What are the steps/strategies to do Active Listening? (Minimum 6 points)

1. Focus on the speaker and topics said.
2. Avoid destructed by own thought.
3. Don't start planning what to say next.
4. Listen without judging,or jumping to conclusions.
5. Don't interrupt.
6. ParaPhrase and Summarise.

### 2. Reflective Listening

### Question 2

#### According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

1. Listen to the speaker's message.
2. Analyze the speaker's message.
3. Reflect the message back to the speaker.
4. Confirm that you properly understood the message.


### 3. Reflection

### Question 3

#### What are the obstacles in your listening process?

1. Comparision and evaluation of what the other person is saying relative.
2. Agreeing just to stop or avoid the function.
3. Making assumptions or reading the mind of the speaker.
4. Judgment of the speaker or the topic.
5. Language barriers:It can exist when there is a language difference between the two individuals talking or when one person has a poor understanding of the spoken language.

### Question 4

#### What can you do to improve your listening?

1. To improve Communication and Interpersonal skills.
2. Visualize what the speaker is saying.
3. Maintain eye contact with the speaker.
4. Limit judgements.
5. Wait for a pause to ask questions.
6. Empathize with the speaker.
7. keep an open mind.

## 4. Types of communication 

### Question 5

#### When do you switch to Passive communication style in your day to day life?

1. Passive communication is a type of communication in which a person sets aside his desires and needs and instead focuses on the needs, desires and opinions of others.
2. To Avoid conflicts
3. If we are feeling fearful that we are about to be harmed, passive communication may help to difuse the situation.

### Question 6

#### When do you switch into Aggressive communication styles in your day to day life?

1. Rebuilding communication is even more challenging for people in recovery addiction.
2. Frequently feel frustrated and irritable and often act aggressively towards others.

### Question 7

#### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

1. Passive-aggressive communication involves expressing negative feelings indirectly rather than openly addressing them. 
2. Passive-Aggresive Communication is a style in which individuals appear passive on the surface but are really acting out anger in a subtle, indirect, or behind-the-scenes way.

### Question 8

#### How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)

* Assertive comunication is expressing our feelings and needs clearly and honestly ,while respecting the feelings and needs of others

1. Reharse what you want to say.
2. use body language.
3. keep emotions in check.
4. start small.
5. Don't be guilty.
6. Take time.






