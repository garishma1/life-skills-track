# Solid Principles

## The First Five Principles Of Solid

### SOLID

* S - Single Responsiblity Principle
* O - Open Closed Principle
* L - Liskov substitution Principle
* I - Interface Segregation Principle
* D - Dependency Inversion Principle

## 1. Single Responsibilty Principle

* A class should have one and only one reason to change, meaning that a class should have only one job.

* One class should only serve one purpose, this does not imply that each class should have only one method but they should all relate directly to the responsibility of the class.

* All the methods and properties should all work towards the same goal. 

* When a class serves multiple purposes or responsibility then it should be made into a new class.

## Code Sample:

## To Calculate the area of square

```js

function calculateSquareArea(sideLength) {
    return sideLength * sideLength;
}
let sideLength = 5;
let area = calculateSquareArea(sideLength);
console.log("Area of the square:", area);

```

## 2. Open Closed Principle

* Entities should be open for extension, but closed for modification.

* Software entities (classes, modules, functions, etc.) be extendable without actually changing the contents of the class you're extending.

* If we could follow this principle strongly enough, it is possible to then modify the behavior of our code without ever touching a piece of original code.

```js

class AreaCalculator {
    static calculate(shape) {
        if (shape instanceof Square) {
            return shape.sideLength ** 2;
        } else if (shape instanceof Circle) {
            return Math.PI * shape.radius ** 2;
        } else {
            throw new Error("Unsupported shape");
        }
    }
}

class Square {
    constructor(sideLength) {
        this.sideLength = sideLength;
    }
}

class Circle {
    constructor(radius) {
        this.radius = radius;
    }
}


let square = new Square(5);
let  circle = new Circle(3);

console.log("Area of the square:", AreaCalculator.calculate(square));
console.log("Area of the circle:", AreaCalculator.calculate(circle));

```
## 3. Liskov Substitution Principle

* Liskov Substitution Principle states:

* Let q(x) be a property provable about objects of x of type T. Then q(y) should be provable for objects y of type S where S is a subtype of T.

* This means that every subclass or derived class should be substitutable for their base or parent class.

## Building off the example AreaCalculator class, consider a new VolumeCalculator class that extends the AreaCalculator class:

```js

class AreaCalculator {
    static calculate(shape) {
        if (shape instanceof Square) {
            return shape.sideLength ** 2;
        } else if (shape instanceof Circle) {
            return Math.PI * shape.radius ** 2;
        } else {
            throw new Error("Unsupported shape");
        }
    }
}

class VolumeCalculator extends AreaCalculator {
    static calculate(shape) {
        if (shape instanceof Cube) {
            return shape.sideLength ** 3;
        } else if (shape instanceof Cylinder) {
            return Math.PI * shape.radius ** 2 * shape.height;
        } else {
            return super.calculate(shape);
        }
    }
}

class Square {
    constructor(sideLength) {
        this.sideLength = sideLength;
    }
}

class Circle {
    constructor(radius) {
        this.radius = radius;
    }
}

class Cube {
    constructor(sideLength) {
        this.sideLength = sideLength;
    }
}

class Cylinder {
    constructor(radius, height) {
        this.radius = radius;
        this.height = height;
    }
}


let square = new Square(5);
let circle = new Circle(3);
let cube = new Cube(4);
let cylinder = new Cylinder(2, 6);

console.log("Area of the square:", AreaCalculator.calculate(square));
console.log("Area of the circle:", AreaCalculator.calculate(circle));
console.log("Volume of the cube:", VolumeCalculator.calculate(cube));
console.log("Volume of the cylinder:", VolumeCalculator.calculate(cylinder));

```

## 4. Interface Segregation Principle

* A Client should not be forced to implement an interface that it doesn't use.

* This rule means that we should break our interfaces in many smaller ones, so they better satisfy the exact needs of our clients.

```js

class Shape2DInterface {
    calculateArea() {
        throw new Error("Method not implemented");
    }
}

class Shape3DInterface {
    calculateVolume() {
        throw new Error("Method not implemented");
    }
}


class Square extends Shape2DInterface {
    constructor(sideLength) {
        super();
        this.sideLength = sideLength;
    }

    calculateArea() {
        return this.sideLength ** 2;
    }
}

class Circle extends Shape2DInterface {
    constructor(radius) {
        super();
        this.radius = radius;
    }

    calculateArea() {
        return Math.PI * this.radius ** 2;
    }
}


class Cuboid extends Shape3DInterface {
    constructor(length, width, height) {
        super();
        this.length = length;
        this.width = width;
        this.height = height;
    }

    calculateVolume() {
        return this.length * this.width * this.height;
    }
}

class Spheroid extends Shape3DInterface {
    constructor(radius1, radius2, radius3) {
        super();
        this.radius1 = radius1;
        this.radius2 = radius2;
        this.radius3 = radius3;
    }

    calculateVolume() {
        return (4 / 3) * Math.PI * this.radius1 * this.radius2 * this.radius3;
    }
}


let square = new Square(5);
let circle = new Circle(3);
let cuboid = new Cuboid(2, 3, 4);
let spheroid = new Spheroid(3, 4, 5);

console.log("Area of the square:", square.calculateArea());
console.log("Area of the circle:", circle.calculateArea());
console.log("Volume of the cuboid:", cuboid.calculateVolume());
console.log("Volume of the spheroid:", spheroid.calculateVolume());

```

## 5. Dependency Inversion Principle

* High-level modules should not depend on low-level modules. Both should depend on abstractions.

* Abstractions should not depend on details. Details should depend on abstractions.

 Or simply : Depend on Abstractions not on concretions

* By applying the Dependency Inversion the modules can be easily changed by other modules just changing the dependency

 module and High-level module will not be affected by any changes to the Low-level module.

## Here is an example of a PasswordReminder that connects to a MySQL database:

```js

class DatabaseConnection {
    connect() {
        throw new Error("Method not implemented");
    }

    disconnect() {
        throw new Error("Method not implemented");
    }

    query(sql) {
        throw new Error("Method not implemented");
    }

class MySQLDatabaseConnection extends DatabaseConnection {
    connect() {
        console.log("Connected to MySQL database");
    }

    disconnect() {
        console.log("Disconnected from MySQL database");
    }

    query(sql) {
        console.log("Executing MySQL query:", sql);
        return "Simulated result";
    }
}

class PasswordReminder {
    constructor(databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    remindUser(userId) {
        
        this.databaseConnection.connect();
        
         let sql = `SELECT email FROM users WHERE id = ${userId}`;
        
         let result = this.databaseConnection.query(sql);
        
        
        console.log(`Reminder email sent to: ${result}`);
        
        
        this.databaseConnection.disconnect();
    }
}

let mySQLDatabaseConnection = new MySQLDatabaseConnection();
let passwordReminder = new PasswordReminder(mySQLDatabaseConnection);
passwordReminder.remindUser(123);
}
```

## References Section :

## Solid Principles Introduction Reference Video

[https://www.youtube.com/watch?v=HLFbeC78YlU&list=PL6n9fhu94yhXjG1w2blMXUzyDrZ_eyOme&index=1]

## 1. Single Responsibilty Principle Reference Video

[https://www.youtube.com/watch?v=hGf2upfDpdo&list=PL6n9fhu94yhXjG1w2blMXUzyDrZ_eyOme&index=2&t=27s]

## 2. Open Closed Principle reference video

[https://www.youtube.com/watch?v=wo06oCBuYYI&list=PL6n9fhu94yhXjG1w2blMXUzyDrZ_eyOme&index=4&t=169s]

## 3. Liskov Substitution Principle reference video

[https://www.youtube.com/watch?v=gnKx1RW_2Rk&list=PL6n9fhu94yhXjG1w2blMXUzyDrZ_eyOme&index=5]

## 4. Interface Segregation principle Reference Video

[https://www.youtube.com/watch?v=CWrRwC8iB30&list=PL6n9fhu94yhXjG1w2blMXUzyDrZ_eyOme&index=3]

## 5. Dependency Inversion Principle

[https://www.youtube.com/watch?v=5WHKNOTqwsA&list=PL6n9fhu94yhXjG1w2blMXUzyDrZ_eyOme&index=6]










